import '../src/consecutive_letters.dart';
import 'package:test/test.dart';

void main() {
  group('Basic tests', () {
    test("testing for 'abc'", () => expect(solve('abc'), true));
    test("testing for 'abd'", () => expect(solve('abd'), false));
    test("testing for 'dabc'", () => expect(solve('dabc'), true));
    test("testing for 'abbc'", () => expect(solve('abbc'), false));
    test("testing for 'v'", () => expect(solve('v'), true));
  });
}
