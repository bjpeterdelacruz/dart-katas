import '../src/hello_world.dart';
import 'package:test/test.dart';
import 'dart:io';

void main() {
  group('Hello World Without strings', () {
    var userCode =
        File('/Volumes/Book/Documents/Documents/Programming/Projects/'
                'Git/Dart/katas/src/hello_world.dart')
            .readAsStringSync();
    test("Doesn't use double quotes", () {
      expect(userCode.contains('"'), equals(false));
    });
    test("Doesn't use single quotes", () {
      expect(userCode.contains("'"), equals(false));
    });
    test("Returns 'Hello, World!'", () {
      expect(helloWorld(), equals('Hello, World!'));
    });
  });
}
