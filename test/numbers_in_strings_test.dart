import '../src/numbers_in_strings.dart';
import 'package:test/test.dart';

void main() {
  group('Fixed tests', () {
    test('Testing for gh12cdy695m1',
        () => expect(solve('gh12cdy695m1'), equals(695)));
    test('Testing for 2ti9iei7qhr5',
        () => expect(solve('2ti9iei7qhr5'), equals(9)));
    test('Testing for vih61w8oohj5',
        () => expect(solve('vih61w8oohj5'), equals(61)));
    test('Testing for f7g42g16hcu5',
        () => expect(solve('f7g42g16hcu5'), equals(42)));
    test('Testing for lu1j8qbbb85',
        () => expect(solve('lu1j8qbbb85'), equals(85)));
  });
}
