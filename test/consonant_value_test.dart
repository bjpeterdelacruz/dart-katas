import '../src/consonant_value.dart';
import 'package:test/test.dart';

void main() {
  group('Fixed tests:', () {
    test('zodiac', () => expect(solve('zodiac'), equals(26)));
    test('chruschtschov', () => expect(solve('chruschtschov'), equals(80)));
    test('khrushchev', () => expect(solve('khrushchev'), equals(38)));
    test('strength', () => expect(solve('strength'), equals(57)));
    test('catchphrase', () => expect(solve('catchphrase'), equals(73)));
    test('twelfthstreet', () => expect(solve('twelfthstreet'), equals(103)));
    test('mischtschenkoana',
        () => expect(solve('mischtschenkoana'), equals(80)));
    test('az', () => expect(solve('az'), equals(26)));
    test('(empty string)', () => expect(solve(''), equals(0)));
    test('aiea', () => expect(solve('aiea'), equals(0)));
  });
}
