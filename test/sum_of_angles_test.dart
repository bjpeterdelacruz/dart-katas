import '../src/sum_of_angles.dart';
import 'package:test/test.dart';

void main() {
  void tester(int n, int exp) =>
      test('Testing for $n', () => expect(angle(n), equals(exp)));
  group('basic tests', () {
    tester(3, 180);
    tester(4, 360);
    tester(5, 540);
  });
}
