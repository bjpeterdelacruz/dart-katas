import '../src/diagonal_strings.dart';
import 'package:test/test.dart';

void main() {
  test('DiagonalsOfSquare', () {
    expect(DiagonalsOfSquare(null), null);
    expect(DiagonalsOfSquare([]), null);
    expect(DiagonalsOfSquare(['abcd', 'kata', '1234', 'qwer']),
        ['aae4', 'kw3d', '1btr', 'q2ca']);
    expect(DiagonalsOfSquare(['abcd', 'kata', '1234', 'qwe']), null);
  });
}
