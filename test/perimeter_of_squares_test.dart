import '../src/perimeter_of_squares.dart';
import 'package:test/test.dart';

void main() {
  test('perimeter', () {
    expect(perimeter(5), BigInt.from(80));
    expect(perimeter(7), BigInt.from(216));
    expect(perimeter(20), BigInt.from(114624));
    expect(
        perimeter(500),
        BigInt.parse(
            '2362425027542282167538999091770205712168371625660854753765546783141099308400948230006358531927265833165504'));
  });
}
