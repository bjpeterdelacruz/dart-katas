int solve(String s) {
  var str = s.replaceAll(RegExp(r'[^0-9]'), ' ').trim().split(RegExp(r'\s+'));
  var numbers = str.map((e) => int.parse(e)).toList();
  numbers.sort();
  return numbers[numbers.length - 1];
}
