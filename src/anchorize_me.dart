String anchorize(String text) {
  var protocols = <String>['http:', 'https:', 'ftp:', 'ftps:', 'file:', 'smb:'];
  var strings = text.split(' ');
  var result = <String>[];
  for (var idx = 0; idx < strings.length; idx++) {
    var found = false;
    for (var idx2 = 0; idx2 < protocols.length && !found; idx2++) {
      var currentString = strings[idx].toLowerCase();
      var currentProtocol = protocols[idx2].toLowerCase();
      if (currentString.indexOf(currentProtocol) == 0) {
        result.add('<a href="' + strings[idx] + '">' + strings[idx] + '</a>');
        found = true;
      }
    }
    if (!found) {
      result.add(strings[idx]);
    }
  }
  return result.join(' ');
}
