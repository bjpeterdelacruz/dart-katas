int solve(String s) {
  var alphabet = 'abcdefghijklmnopqrstuvwxyz';
  var vowels = 'aeiou';
  var strings = <String>[];
  for (var startIndex = 0, endIndex = 0; endIndex <= s.length; endIndex++) {
    if (endIndex == s.length && startIndex < s.length) {
      strings.add(s.substring(startIndex, endIndex));
      break;
    }
    if (endIndex < s.length && vowels.contains(s[endIndex])) {
      var substring = s.substring(startIndex, endIndex);
      if (substring.isNotEmpty) {
        strings.add(substring);
      }
      startIndex = endIndex + 1;
    }
  }
  var sum = 0;
  for (var string in strings) {
    var currentSum = 0;
    for (var idx = 0; idx < string.length; idx++) {
      currentSum += alphabet.indexOf(string[idx]) + 1;
    }
    if (currentSum > sum) {
      sum = currentSum;
    }
  }
  return sum;
}
