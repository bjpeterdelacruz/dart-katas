String solve(String s) {
  var upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var lower = 'abcdefghijklmnopqrstuvwxyz';

  var countLower = 0;
  var countUpper = 0;

  for (var idx = 0; idx < s.length; idx++) {
    if (upper.contains(s[idx])) {
      countUpper++;
    } else if (lower.contains(s[idx])) {
      countLower++;
    }
  }

  return countLower < countUpper ? s.toUpperCase() : s.toLowerCase();
}
