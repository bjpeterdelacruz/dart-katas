import 'package:collection/collection.dart';

bool solve(String s) {
  var letters = s.split('');
  groupBy(letters, (string) => string).forEach((key, value) {
    if (value.length > 1) {
      return false;
    }
  });
  letters.sort();
  return 'abcdefghijklmnopqrstuvwxyz'.contains(letters.join(''));
}
