List<int> pendulum(List<int> values) {
  values.sort();
  var sorted = values[0].toString();
  for (var idx = 1; idx < values.length; idx++) {
    if (idx % 2 == 1) {
      sorted += ',' + values[idx].toString();
    } else {
      sorted = values[idx].toString() + ',' + sorted;
    }
  }
  return sorted.split(',').map((e) => int.parse(e)).toList();
}
