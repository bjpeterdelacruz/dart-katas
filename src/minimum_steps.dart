int minimumSteps(List<int> nums, int value) {
  nums.sort();
  var count = 0;
  for (var idx = 1, sum = nums[0];
      idx < nums.length && sum < value;
      idx++, count++) {
    sum += nums[idx];
  }
  return count;
}
