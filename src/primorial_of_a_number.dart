import 'dart:math';

int numPrimorial(int n) {
  var numbers = <int>[2];
  for (var i = 3; numbers.length != n; i++) {
    if (isPrime(i)) {
      numbers.add(i);
    }
  }
  return numbers.reduce((value, element) => value * element);
}

bool isPrime(int n) {
  if (n % 2 == 0) {
    return false;
  }
  var result = sqrt(n).floor();
  for (var i = 3; i <= result; i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
