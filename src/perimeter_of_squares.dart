BigInt perimeter(int number) {
  return BigInt.from(4) * fib(number);
}

BigInt fib(int number) {
  var sums = <BigInt>[BigInt.from(0), BigInt.from(1)];
  for (var index = 2; index <= number + 1; index++) {
    sums.add(sums[index - 1] + sums[index - 2]);
  }
  return sums.fold(BigInt.from(0), (a, b) => a + b);
}
