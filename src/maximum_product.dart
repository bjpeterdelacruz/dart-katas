int adjacentElementsProduct(List<int> array) {
  var maxProduct = array[0] * array[1];
  for (var idx = 2; idx < array.length; idx++) {
    if (array[idx - 1] * array[idx] > maxProduct) {
      maxProduct = array[idx - 1] * array[idx];
    }
  }
  return maxProduct;
}
