List<int> arrayLeaders(numbers) {
  var results = <int>[];
  for (var sum = 0, idx = numbers.length - 1; idx >= 0; idx--) {
    if (numbers[idx] > sum) {
      results.insert(0, numbers[idx]);
    }
    sum += numbers[idx];
  }
  return results;
}
