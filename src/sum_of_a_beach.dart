int sumOfABeach(String beach) {
  var words = ['sand', 'water', 'fish', 'sun'];
  var count = 0;
  var b = beach.toLowerCase();
  for (var word in words) {
    while (b.contains(word)) {
      b = b.replaceFirst(word, '');
      count++;
    }
  }
  return count;
}
