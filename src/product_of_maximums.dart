int maxProduct(List<int> arr, int size) {
  arr.sort();
  var product = 1;
  for (var count = 0, idx = arr.length - 1;
      idx >= 0 && count < size;
      idx--, count++) {
    product *= arr[idx];
  }
  return product;
}
