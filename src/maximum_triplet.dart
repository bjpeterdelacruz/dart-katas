int maxTriSum(List<int> nums) {
  var uniqueNums = nums.toSet().toList();
  uniqueNums.sort();
  var lastIdx = uniqueNums.length - 1;
  return uniqueNums[lastIdx] +
      uniqueNums[lastIdx - 1] +
      uniqueNums[lastIdx - 2];
}
