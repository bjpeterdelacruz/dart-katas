List<int> incrementer(List<int> nums) {
  var results = <int>[];
  for (var idx = 0; idx < nums.length; idx++) {
    var result = nums[idx] + idx + 1;
    if (result > 9) {
      var str = result.toString();
      result = int.parse(str.substring(str.length - 1));
    }
    results.add(result);
  }
  return results;
}
