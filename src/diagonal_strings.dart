class DiagonalString {
  final int position;
  final String string;
  String diagonalString;
  DiagonalString(this.position, this.string);
  @override
  String toString() {
    return [position, string, diagonalString].toString();
  }
}

List<String> DiagonalsOfSquare(List<String> array) {
  if (array == null || array.isEmpty) {
    return null;
  }

  var diagonalStrings = <DiagonalString>[];
  for (var index = 0; index < array.length; index++) {
    if (array[index].length != array.length) {
      return null;
    }
    diagonalStrings.add(DiagonalString(index, array[index]));
  }

  diagonalStrings.sort((var a, var b) => a.string.compareTo(b.string));

  for (var count = 0; count < array.length; count++) {
    generateDiagonalString(diagonalStrings, count);
  }

  diagonalStrings.sort((var a, var b) => a.position - b.position);

  return diagonalStrings.map((e) => e.diagonalString).toList();
}

void generateDiagonalString(List<DiagonalString> simpleStrings, int start) {
  var string = '';
  for (var cnt = 0, idx = start; cnt < simpleStrings.length; cnt++, idx++) {
    if (idx == simpleStrings.length) {
      idx = 0;
    }
    string += simpleStrings[idx].string[cnt];
  }
  simpleStrings[start].diagonalString = string;
}
